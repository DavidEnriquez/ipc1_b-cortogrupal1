/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corto1;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javafx.scene.control.ComboBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;

/**
 *
 * @author davidomarenriquezreyes
 */
public class Ventana extends JFrame {
    public Ventana(){
        this.setTitle("On tas mis pinshis puntos extra");
//        Button b = new Button("JO");
        Panel p = new Panel();
//        p.add(b);
        this.setBounds(0,0,1000,500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.setUndecorated(true);
        this.setBackground(Color.BLACK.darker());
        this.setOpacity((float) 0.70);
//        String [] comboOptions = new String[100];
       
        JPanel jpanel = new JPanel(new GridBagLayout());

        this.add(p);
//        JComboBox jc = new JComboBox();
        JTabbedPane jtp = new JTabbedPane();
        jtp.setPreferredSize(new Dimension(600,400));
        JComboBox cb =  new JComboBox();
        Button cbA = new Button("Ir a la ventana");

//        jtp.add(jpanel);
        
        Button bAdd = new Button("Agregar pestana");
        bAdd.setBackground(Color.GREEN);
        Button bDelete = new Button("Eliminar pestana");
        bDelete.setBackground(Color.RED);
        Button bAccionP = new Button("Accion Pro");
        bAccionP.setBackground(Color.ORANGE);
                
        bAdd.addActionListener(new ActionListener(){
        @Override
            public void actionPerformed(ActionEvent e) {
                JPanel jp = new JPanel(new GridBagLayout());
                GridBagConstraints c = new GridBagConstraints();

                JTextPane jtp2 = new JTextPane();
                Button bAccionV = new Button("Accion Ventana");
                bAccionV.setBounds(10,10,10,10);
                jtp2.setBounds(0,0,10,20);
                jtp2.setBackground(Color.WHITE);
                jtp2.setEditable(true);
//                jp.add(jtp2);
//                jp.add(bAccionV);


                    c.fill = GridBagConstraints.HORIZONTAL;
                    c.anchor = GridBagConstraints.NORTH;
                    c.gridx = 0;
                    c.gridy = 0;
                    jp.add(jtp2,c);
                    c.gridx = 0;
                    c.gridy = 1;
                    jp.add(bAccionV,c);
                    JTextField jtf = new JTextField();
                    jtf.setBounds(0, 0, 20,10);
                    jtf.setText(""+jtp.getSelectedIndex());
//                    comboOptions[jtp.getSelectedIndex()+1] = ""+jtp.getSelectedIndex();
//                    cb.add(""+jtp.getSelectedIndex(), jtf);
                    cb.addItem((Object)(""+jtp.getTabCount()));
                    
                    
                    cb.addActionListener (new ActionListener () {
                        public void actionPerformed(ActionEvent e) {
                            jtp.setSelectedIndex(cb.getSelectedIndex());
                        }
                    });
                    
                    jtp.addTab(""+jtp.getTabCount(), jp);
                    bAccionV.addActionListener(new ActionListener(){
                    @Override
                        public void actionPerformed(ActionEvent e) {
                            String s = "";

                            JPanel p = (JPanel)jtp.getComponent(jtp.getSelectedIndex());
            //                            p.getComponentAt(0,0);
//                                System.out.println(p);
                            JTextPane jtp3 = (JTextPane)p.getComponent(0);
//                            JTextPane jtp3 = (JTextPane)p.getComponentAt(0,0);;
                            s = jtp3.getText();
                            JFrame jfEspecific = new JFrame();
                            jfEspecific.setBounds(0,0,200,100);
                            jfEspecific.setTitle("Aqui sta tu pinshi contenido");
                            jfEspecific.setLocationRelativeTo(null);
                            JTextField jtfe = new JTextField();
                            jtfe.setText("Panel: "+jtp.getSelectedIndex()+" - Contenido: "+s);
                            jfEspecific.add(jtfe);
                            jfEspecific.show(true);
//                            JOptionPane.showMessageDialog(null, "Panel: "+jtp.getSelectedIndex()+" - Contenido: "+s);
                        }
                   });



                
            }
       });
        bDelete.addActionListener(new ActionListener(){
        @Override
            public void actionPerformed(ActionEvent e) {
                try{
                    jtp.remove(jtp.getSelectedIndex());                    
                }catch(Exception i){
                    JFrame jfEspecific = new JFrame();
                    jfEspecific.setBounds(0,0,550,100);
                    jfEspecific.setTitle("Nell prro >:v");
                    jfEspecific.setLocationRelativeTo(null);
                    JTextField jtfe = new JTextField();
                    jtfe.setText("No puede eliminar pestañas si no hay ninguna, te mamas Kevin :v");
                    jfEspecific.add(jtfe);
                    jfEspecific.show(true);                    
                }
                
            }
       });
        bAccionP.addActionListener(new ActionListener(){
        @Override
            public void actionPerformed(ActionEvent e) {
                                 String s = "";

                            JPanel p = (JPanel)jtp.getComponent(jtp.getSelectedIndex());
//            //                            p.getComponentAt(0,0);
//                                System.out.println(p);
                            JTextPane jtp3 = (JTextPane)p.getComponent(0);
//                            JTextPane jtp3 = (JTextPane)p.getComponentAt(0,0);
                            s = jtp3.getText();
//                            JOptionPane.showMessageDialog(null, "Panel: "+jtp.getSelectedIndex()+" - Contenido: "+s);
                            JFrame jfEspecific = new JFrame();
                            jfEspecific.setBounds(0,0,200,100);
                            jfEspecific.setTitle("Aqui sta tu pinshi contenido");
                            jfEspecific.setLocationRelativeTo(null);
                            JTextField jtfe = new JTextField();
                            jtfe.setText("Panel: "+jtp.getSelectedIndex()+" - Contenido: "+s);
                            jfEspecific.add(jtfe);
                            jfEspecific.show(true);


            }
       });
        cbA.addActionListener(new ActionListener(){
        @Override
            public void actionPerformed(ActionEvent e) {
                                 String s = "";

                            JPanel p = (JPanel)jtp.getComponent(jtp.getSelectedIndex());
//            //                            p.getComponentAt(0,0);
//                                System.out.println(p);
                            JTextPane jtp3 = (JTextPane)p.getComponent(0);
//                            JTextPane jtp3 = (JTextPane)p.getComponentAt(0,0);
                            s = jtp3.getText();
//                            JOptionPane.showMessageDialog(null, "Panel: "+jtp.getSelectedIndex()+" - Contenido: "+s);
                            JFrame jfEspecific = new JFrame();
                            jfEspecific.setBounds(0,0,200,100);
                            jfEspecific.setTitle("Aqui sta tu pinshi contenido");
                            jfEspecific.setLocationRelativeTo(null);
                            JTextField jtfe = new JTextField();
                            jtfe.setText("Panel: "+jtp.getSelectedIndex()+" - Contenido: "+s);
                            jfEspecific.add(jtfe);
                            jfEspecific.show(true);


            }
       });
//       jpanel.add(bAdd);
//       jpanel.add(bDelete);
       JPanel jpes = new JPanel(new GridBagLayout());
       GridBagConstraints c = new GridBagConstraints();
       jpes.add(jtp,c);

       
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.NORTH;
        c.gridx = 0;
        c.gridy = 0;
        jpanel.add(bAdd,c);
        c.gridx = 1;
        jpanel.add(bDelete,c);
        c.gridx = 2;
        jpanel.add(bAccionP);
//        c.gridx = 1;
//        c.ipadx = 200;
//        jpanel.add(jtp,c);
        c.gridx = 0;
        c.gridy = 1;
        jpanel.add(cb,c);
        c.gridx = 1;
        c.gridy = 1;
        jpanel.add(cbA,c);
        c.gridx = 0;
        c.gridy = 2;
        jpanel.add(jtp,c);
//        this.setMenuBar();
       this.add(jpanel);
//       this.add(jpes);
//       this.add(jtp);
//        jpanel.add(jc);
        this.show();
    }
}
